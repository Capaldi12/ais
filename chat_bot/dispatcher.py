from . import api
from .message import Message

import schedule

from contextlib import contextmanager

import time


class Dispatcher(object):
    """
    Handles dispatching messages to registered command handlers

    Method `handle()` can be used as decorator to register handlers
    """
    _commands: dict
    _fallback: callable
    _unknown: callable

    def __init__(self):
        self._commands = {}
        self._unknown = lambda msg: msg.answer('Unknown command')
        self._fallback = lambda msg: None
        self.offset = None

        self.data = None

    def __call__(self, data):
        self.data = data

        @contextmanager
        def context():
            self.offset = data['config']['offset']

            try:
                yield None

            finally:
                self.data['config']['offset'] = self.offset

        return context()

    def handler(self, *commands, unknown=False):
        def decorator(func):
            if len(commands) == 0:
                if unknown:
                    self._unknown = func

                else:
                    self._fallback = func

            else:
                self._commands.update({command: func for command in commands})

            return func

        return decorator

    def poll(self, delay=100):
        delay /= 1000
        print('Start polling')
        try:
            while True:
                for update in api.get_updates(self.offset):
                    message = Message(update['message'])

                    if message.is_command:
                        cmd = message.command

                        self._commands.get(cmd, self._unknown)(message)
                    else:
                        self._fallback(message)

                    self.offset = update['update_id'] + 1

                schedule.run_pending()
                time.sleep(delay)

        except KeyboardInterrupt:
            print('End polling')
            return
