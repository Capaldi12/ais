"""
Run this to start the bot
"""

from . import dispatcher, data_handler, handlers, scheduler


with data_handler as data, dispatcher(data):
    handlers.register(data, dispatcher)
    scheduler.register(data, data_handler)

    dispatcher.poll()
