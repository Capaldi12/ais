from . import api

from datetime import datetime
from types import SimpleNamespace
import re

from functools import lru_cache as cache  # just in case


class Message(object):
    """
    Wrapper for message JSON object returned by Telegram API

    Provides easy access to fields (via `.`) and
    methods to reply/answer without direct call API
    """
    def __init__(self, json):
        self._json = json

    @property
    @cache
    def id(self) -> int:
        return self._json['message_id']

    @property
    @cache
    def text(self) -> str:
        return self._json.get('text', '')

    @property
    @cache
    def date(self) -> datetime:
        return datetime.fromtimestamp(self._json['date'])

    @property
    @cache
    def chat(self):
        return SimpleNamespace(**self._json['chat'])

    @property
    @cache
    def from_user(self):
        return SimpleNamespace(**self._json['from'])

    @property
    @cache
    def entities(self):
        e = self._json.get('entities', None)

        if e is None or len(e) == 0:
            return None

        return SimpleNamespace(**e[0])

    @property
    @cache
    def is_command(self):
        return self.entities is not None and self.entities.type == 'bot_command'

    @property
    @cache
    def command(self) -> str:
        return re.findall(r'(?!^/)\w+', self.text)[0]

    @property
    @cache
    def rest(self) -> str:
        return re.sub(r'^/\w+\s*', '', self.text)

    _arg_pattern = re.compile(r'(?<!\S)(-[a-z])\b(.+?)(?=(?<!\S)-[a-z]\b|$)')

    @property
    @cache
    def arguments(self) -> dict:
        return {match.group(1): match.group(2).strip()
                for match in self._arg_pattern.finditer(self.text)}

    def answer(self, text, **params) -> bool:
        return api.send_message(self.chat.id, text, **params)

    def reply(self, text, **params) -> bool:
        return api.send_message(self.chat.id, text, **params,
                                reply_to_message_id=self.id)
