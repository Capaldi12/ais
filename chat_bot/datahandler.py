from . import functions

import json
import os


class DataHandler(object):
    """
    Handles save-load of data to JSON file. Can be used as context manager
    """
    def __init__(self, path):
        self._path = path
        self._data = {}

    def __enter__(self):
        self.load()

        return self._data

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save()

    def save(self):
        with open(self._path, 'w', encoding='utf-8') as file:
            json.dump(self._data, file, indent=2)

    def load(self):
        if not os.path.exists(self._path):
            self._data.update(functions.make_data())

        else:
            with open(self._path, 'r', encoding='utf-8') as file:
                self._data.update(json.load(file))
