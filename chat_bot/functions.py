import re

flag = re.compile(r'(?<![-\w])(-[a-z])\b')


def make_data():
    return {
        'config': {
            'offset': 0,
            'date_format': '%d.%m.%Y'
        },

        'users': {

        }
    }


def make_user(user_id, username):
    return {
        'id': user_id,
        'username': username,
        'dates': [

        ]
    }


def make_date(who, when, where):
    return {
        'person': who,
        'date': when,
        'address': where
    }
